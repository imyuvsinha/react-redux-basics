import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import reportWebVitals from "./reportWebVitals";
import { createStore } from "redux";
import allReducers from "./reducers";
import { Provider } from "react-redux";

// [1]. STORE --> Globalized State
const store = createStore(
  allReducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
// [2].ACTION --> WHAT YOU WANT TO DO(INCREMENT)

///////--->FOR  EXAMPLE , IF WE HAVE INCREMENT METHOD TO INCREMENT ANY STATE VALUE

//[3]. REDUCER -->DESCRIBES HOW YOUR ACTIONS TRANSFORM THE STATE INTO NEXT STATE
// -->REDUCER CHECK WHICH ACTION YOU DID AND BASED ON THE ACTION IT'S GONNA MODIFY OUR STORE(STATES).

///Display in console

//[4]. DISPATCH -->WHERE WE CAN ACTUALLY EXECUTE THAT ACTION.
//[HOW IT WORKS???] -->1.DISPATCH THE ACTION(INCREMENT) TO THE REDUCER,SO WE CAN SEND THAT ACTION TO THE REDUCER,
//-->2. AND THEN REDUCER GONNA CHECK WHAT TO DO
//-->3. AND THEN STORE(STATES) GETS UPDATED.

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <App />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
