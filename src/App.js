import "./App.css";
import { useSelector, useDispatch } from "react-redux";
import { increment, decrement, loggedIn } from "./actions";
function App() {
  const counter = useSelector((state) => state.counter);
  const isLog = useSelector((state) => state.isLogged);
  const dispatch = useDispatch();

  return (
    <div className="App">
      <h1>Counter:{counter}</h1>
      <button onClick={() => dispatch(increment(10))}>+</button>
      <button onClick={() => dispatch(decrement())}>-</button>
      {isLog ? <h3>heloo user</h3> : ""}
      <button onClick={() => dispatch(loggedIn())}>Login</button>
    </div>
  );
}

export default App;
