// HERE WE HAVE TO DEFINE WHAT ARE THE TYPES OF ACTIONS WE HAVE TO PERFORM::

export const increment = (incrementedBy) => {
  return {
    type: "INCREMENT",
    payload: incrementedBy,
  };
};

export const decrement = () => {
  return {
    type: "DECREMENT",
  };
};

export const loggedIn = () => {
  return {
    type: "SIGN_IN",
  };
};
